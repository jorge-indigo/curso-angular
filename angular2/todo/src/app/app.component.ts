import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  contador: number = 0;

  constructor() {
  }

  incrementar(): void {
    this.contador++;
  }

  reset(): void {
    this.contador = 0;
  }
}
