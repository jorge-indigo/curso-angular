import { Todo } from '../domain/todo';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TodoService {

    constructor(private http: Http) { }

    consultarTodos(): Todo[] {
        return [
            { id: 0, texto: 'Ir por el pan', fecha: new Date() },
            { id: 1, texto: 'Comprar boletos del metro', fecha: new Date() },
        ]
    }
}