import { Component } from '@angular/core';
import { Todo } from '../domain/todo';

@Component({
    selector: 'app-agregar-todo',
    templateUrl: 'agregar-todo.component.html'
})
export class AgregarTodoComponent {
    descripcionTodo: string;

    agregarTodo(){
        let t: Todo = new Todo()
        t.id = 1;
        t.texto = this.descripcionTodo;
        t.fecha = new Date()
    }
}