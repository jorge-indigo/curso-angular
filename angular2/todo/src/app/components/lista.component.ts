import { Component } from '@angular/core';
import { Todo } from '../domain/todo';
import * as moment from 'moment';
import { TodoService } from '../services/todo.service';

@Component({
    selector: 'app-lista',
    templateUrl: 'lista.component.html',
    providers: [ TodoService ]
})
export class ListaComponent {
    todos: Todo[];

    constructor(private todoService: TodoService) {
        this.todos = [];
        this.consultarTodos();
    }

    formatearFecha(fecha: Date): string {
        return moment(fecha).format("DD/MM/YYYY HH:mm A")
    }

    consultarTodos() {
        this.todos = this.todoService.consultarTodos();
    }

    eliminarTodo(id: number){
        let todoId = this.todos.findIndex(i => i.id == id);
        this.todos.splice(todoId, 1);
    }
}