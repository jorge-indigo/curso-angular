var f = function () {
    var x = 0; // El alcance es entre las llaves

    if (true) {
        var y = 1;  // El acance es entre las llaves de IF
    }

    try {
        var z = 2;
        throw new Error();
    } catch(e) {
        console.log("Z es", z);
    }

    function otraFuncion() {
        var w = 3;
    }

    otraFuncion();

    console.log("X es", x);
    console.log("Y es", y);
    //console.log("W es", w);
}

f();

console.log("==============================================");

var g = function () {
    let a = 0;

    if (true) {
        let b = 0;
    }

    console.log("A es", a);
    console.log("B es", b);
}

g();


// Al crear una variable con "var", su alcance es
// la función más cercana.

// Al crear una variable con "let", su alcance es
// el bloque de código más cercano.