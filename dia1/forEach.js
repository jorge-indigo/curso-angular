function forEach(arreglo, fn) {
    // Implementar

    // For normal
    for (let i = 0; i < arreglo.length; i++ ) {
        let e = arreglo[i];
    }

    // J es el índice del elemento
    for (let j in arreglo) {
        let e = arreglo[j];
    }

    // K es el elemento
    for (let k of arreglo) {
        let e = k;
    }
}

forEach([1,2,3,4], function (e) {
    console.log(e);
});



// 1
// 2
// ...
// 4