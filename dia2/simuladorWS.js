var usuarios = [];

function consultarUsuarios(callback) {
    setTimeout(function () {
        return callback(usuarios);
    }, 3000);
}

function guardarUsuario(usuario, callback) {
    usuarios.push(usuario);

    setTimeout(function () {
        callback();
    }, 3000);
}

function eliminarUsuario(id, callback) {
    usuarios = usuarios.filter(usuario => usuario.id != id);
    setTimeout(function () {
        callback();
    }, 3000);
}

/* -------------------------------- */

console.log("Ejecutando Webservices");

// Callback Hell
consultarUsuarios(function (listaUsuarios) {
    console.log("Usuarios en BD", listaUsuarios);

    guardarUsuario({ nombre: "Jorge", id: 1 }, function () {
        console.log("El usuario Jorge ya se guardó");

        guardarUsuario({ nombre: "Dulce", id: 2 }, function () {
            console.log("El usuario Dulce ya se guardó");

            guardarUsuario({ nombre: "Natalia", id: 3 }, function () {
                consultarUsuarios(function (listaActualizada) {
                    console.log("Usuarios en BD", listaActualizada);

                    eliminarUsuario(1, function () {
                        console.log("El usuario Jorge se eliminó");

                        consultarUsuarios(function (nuevaLista) {
                            console.log("Usuarios en BD", nuevaLista);
                        });
                    });
                });
            });
        });
    });
});