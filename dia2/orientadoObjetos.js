class Figura {
    constructor(id) {
        this.id;
    }
}

class Rectangulo extends Figura {
    
    constructor(id, alto, ancho) {
        super(id);
        this.alto = alto;
        this._ancho = ancho;
    }

    static tipo() {
        return "Rectangulo";
    }

    set ancho(nuevoAncho) {
        this._ancho = nuevoAncho * 2;
    }

    get ancho() {
        return this._ancho;
    }
}

console.log(Rectangulo.tipo());

var rec = new Rectangulo(0, 20, 10);
console.log(rec.alto);
rec.ancho = 20;
console.log(rec.ancho);