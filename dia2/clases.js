class Animal {

    constructor(nombre) {
        this.nombre = nombre;;
    }

    saluda() {
        console.log("Me llamo", this.nombre);
    }
}

var a = new Animal("Tigrito");
a.saluda();