function Animal() {

    this.camina = camina;
    this.muerete = muerete;

    function camina() {
        console.log("Estoy caminando");
    }

    function muerete() {
        console.log("Ya me morí");
    }
}

function Animal() { }

Animal.prototype.camina = function () {
    console.log("Estoy caminando");
}

Animal.prototype.muerete = function () {
    console.log("Ya me morí");
}

var a = new Animal();
var b = new Animal();

a.camina();
b.camina();
//console.log(a.muerete());



a.muerete();

Array.prototype.duplicar = function () {
    var nuevaLista = [];
    for(let e of this) {
        nuevaLista.push(e * 2);
    }
    return nuevaLista;
}

var x = [1,2,3,4,5]
console.log(x.duplicar());