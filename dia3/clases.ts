class Punto {
    
    x: number;
    y: number;
    private z: number;
    
    constructor() {
        this.x = 0;
        this.y = 0;
    }

    private mover(): void {
        this.z = 0;
    }
}

let p: Punto = new Punto();
console.log(p);