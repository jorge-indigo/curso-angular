interface Punto {
    x: number;
    y: number;
}

// Forma 1 "a la Java"
class PuntoImpl implements Punto {
    x: number;
    y: number;
}

// Forma 2, "la ñera"
let x: Punto = { x: 0, y: 0 };


interface Punto3D {
    x: number;
    y: number;
    z?: number;
    mover?(punto: Punto3D): Punto3D;
}

var punto3D: Punto3D = { 
    x: 0, 
    y:0,
    mover: (p: Punto3D) => ({ 
        x: p.x + this.x, 
        y: this.y + p.y
    })
};