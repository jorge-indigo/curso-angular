function f() {
    console.log("f es evaluado");
    return function (target, propertyKey: string,
    descriptor: PropertyDescriptor) {
        console.log("f es ejecutado");
    }
}

class C {
    @f()
    metodo(){}
}