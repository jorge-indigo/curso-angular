let x: number = 0;
let y: string = "Hola";
let z: boolean = true;
let a: Array<string> = [];
let b: string [] = [];
let c: undefined = undefined;
let d: null = null;
let e: object = {};
let j: Function = function () {}
let k: void = undefined;

let h: any; // let h;

function suma(a: number, b: number): number { return a + b };
console.log(suma(1,2));
console.log(suma("a", true));