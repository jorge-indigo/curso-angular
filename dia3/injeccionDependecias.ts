interface Coordenada {
    n: number;
}

class Punto {
    constructor(private c1: Coordenada, 
    private c2: Coordenada) {
    }
}

let p: Punto = new Punto({n: 0} as Coordenada, {n: 0} as Coordenada);
console.log(p);
