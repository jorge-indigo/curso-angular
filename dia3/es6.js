// ECMAScript 6 (2015)
// ECMAScript es un standard => Javascript, JScript, ActionScript, QScript, etc... 

// let
// Arrow Functions
// Clases y Herencia

var id = 1, nombre = "Jorge";

var json = { id: id, nombre: nombre } // ES5
var json = { id, nombre };

var [x, y] = [0,1] // x -> 0, y -> 1

function f(p = null) {
    console.log(p);
}

f();

function f2({ nombre, id }) {
    console.log(nombre, id);
}

f2({ nombre: "Yisus", id: 0, edad: 2017 });